# squeakymouse
XFCE sandbox repository for Pisi Linux
# How do I set this up
Open your package manager, then add a repository called squeakymouse-xfce.  
Then, copy the link below for the address of the repo:  
https://gitlab.com/squeakymouse-packages/squeakymouse-xfce/raw/master/pisi-index.xml.xz  
Alternatively, you can add the repository from the command line:  
sudo pisi add-repo squeakymouse-xfce https://gitlab.com/squeakymouse-packages/squeakymouse-xfce/raw/master/pisi-index.xml.xz  
Note: this repository works only with Pisi Linux 2.0    
# Caveats
xfce4-mixer is not included as it requires gstreamer-0.10   
# Package sources
If you would like to build the packages yourself, check squeakymouse-xfce-descr 
